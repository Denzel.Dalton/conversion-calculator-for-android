package com.converter.conversioncalculator;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity{


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        final ConversionUnit converter = new ConversionUnit();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final Spinner typeSP = (Spinner) findViewById(R.id.typeSP);
        final EditText inputNumberET = (EditText) findViewById(R.id.inputNumberET);
        final Spinner firstUnitOptionSP = (Spinner) findViewById(R.id.fisrtUnitOptionSP);
        final Spinner secondUnitSP = (Spinner) findViewById(R.id.secondUnitSP);
        final TextView result = (TextView) findViewById(R.id.resultTV);


        final ArrayAdapter<CharSequence> areaAdapter = ArrayAdapter.createFromResource(this,
                R.array.Area_Array, android.R.layout.simple_spinner_item);
        areaAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        final ArrayAdapter<CharSequence> speedAdapter = ArrayAdapter.createFromResource(this,
                R.array.Speed_Array, android.R.layout.simple_spinner_item);
        speedAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        final ArrayAdapter<CharSequence> lengthAdapter = ArrayAdapter.createFromResource(this,
                R.array.Length_Array, android.R.layout.simple_spinner_item);
        lengthAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        final ArrayAdapter<CharSequence> baseAdapter = ArrayAdapter.createFromResource(this,
                R.array.Temp_Array, android.R.layout.simple_spinner_item);
        baseAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        final ArrayAdapter<CharSequence> volAdapter = ArrayAdapter.createFromResource(this,
                R.array.Vol_Array, android.R.layout.simple_spinner_item);
        volAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        final ArrayAdapter<CharSequence> massAdapter = ArrayAdapter.createFromResource(this,
                R.array.Mass_Array, android.R.layout.simple_spinner_item);
        massAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        final ArrayAdapter<CharSequence> angleAdapter = ArrayAdapter.createFromResource(this,
                R.array.Angle_Array, android.R.layout.simple_spinner_item);
        angleAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        final ArrayAdapter<CharSequence> digitalStorageAdapter = ArrayAdapter.createFromResource(this,
                R.array.Digital_Storage_Array, android.R.layout.simple_spinner_item);
        digitalStorageAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        final ArrayAdapter<CharSequence> digitalTransferRate = ArrayAdapter.createFromResource(this,
                R.array.Digital_Transfer_Rate, android.R.layout.simple_spinner_item);
        digitalTransferRate.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        final ArrayAdapter<CharSequence> frequencyAdapter = ArrayAdapter.createFromResource(this,
                R.array.Freq_Array, android.R.layout.simple_spinner_item);
        frequencyAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        ArrayAdapter<CharSequence> typeAdapter = ArrayAdapter.createFromResource(this,
                R.array.Type, android.R.layout.simple_spinner_item);
        typeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        typeSP.setAdapter(typeAdapter);


        firstUnitOptionSP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                result.setText("");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        secondUnitSP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                result.setText("");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        typeSP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String text = parent.getItemAtPosition(position).toString();
                if(text.equals("Length")){
                    firstUnitOptionSP.setAdapter(lengthAdapter);
                    secondUnitSP.setAdapter(lengthAdapter);
                }else if(text.equals("Temp")){
                    firstUnitOptionSP.setAdapter(baseAdapter);
                    secondUnitSP.setAdapter(baseAdapter);
                }else if(text.equals("Volume")){
                    firstUnitOptionSP.setAdapter(volAdapter);
                    secondUnitSP.setAdapter(volAdapter);
                }else if(text.equals("Area")){
                    firstUnitOptionSP.setAdapter(areaAdapter);
                    secondUnitSP.setAdapter(areaAdapter);
                }else if(text.equals("Speed")){
                    firstUnitOptionSP.setAdapter(speedAdapter);
                    secondUnitSP.setAdapter(speedAdapter);
                }else if(text.equals("Digital Storage")){
                    firstUnitOptionSP.setAdapter(digitalStorageAdapter);
                    secondUnitSP.setAdapter(digitalStorageAdapter);
                }else if(text.equals("Data Transfer Rate")){
                    firstUnitOptionSP.setAdapter(digitalTransferRate);
                    secondUnitSP.setAdapter(digitalTransferRate);
                }else if(text.equals("Plane Angle")){
                    firstUnitOptionSP.setAdapter(angleAdapter);
                    secondUnitSP.setAdapter(angleAdapter);
                }else if(text.equals("Mass")){
                    firstUnitOptionSP.setAdapter(massAdapter);
                    secondUnitSP.setAdapter(massAdapter);
                }else if(text.equals("Frequency")){
                    firstUnitOptionSP.setAdapter(frequencyAdapter);
                    secondUnitSP.setAdapter(frequencyAdapter);
                }
                inputNumberET.setText("");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        Button Convert = (Button) findViewById(R.id.button);
        Convert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Double value;
                String firstUnit = firstUnitOptionSP.getSelectedItem().toString();
                String secondUnit = secondUnitSP.getSelectedItem().toString();
                if(inputNumberET.getText().toString().equals("")){
                    return;
                }
                value = Double.parseDouble(inputNumberET.getText().toString());
                switch(typeSP.getSelectedItem().toString()){
                    case "Length":
                        value = converter.convertLength(value, firstUnit, secondUnit);
                        break;
                    case "Volume":
                        value = converter.convertVolume(value, firstUnit, secondUnit);
                        break;
                    case "Area":
                        value = converter.convertArea(value, firstUnit, secondUnit);
                        break;
                    case "Temp":
                        value = converter.convertTemp(value, firstUnit, secondUnit);
                        break;
                    case "Speed":
                        value = converter.convertSpeed(value, firstUnit, secondUnit);
                        break;
                    case "Mass":
                        value = converter.convertMass(value, firstUnit, secondUnit);
                        break;
                    case "Digital Storage":
                        value = converter.convertDigitalStorage(value, firstUnit, secondUnit);
                        break;
                    case "Data Transfer Rate":
                        value = converter.convertDigitalTransfer(value, firstUnit, secondUnit);
                        break;
                    case "Frequency":
                        value = converter.convertFrequency(value, firstUnit, secondUnit);
                    case "Plane Angle":
                        value = converter.convertAngle(value, firstUnit, secondUnit);
                        default:break;

                }
                result.setText(value.toString());
            }
        });
    }
    public double round(double value, int places){
        double scale = Math.pow(10, places);
        return Math.round(value*scale);
    }
}


