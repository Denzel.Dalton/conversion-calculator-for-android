package com.converter.conversioncalculator;


public class ConversionUnit {

    private Double[][] lengthValues={
            {1000.0,1e+6,1e+7,2.54e+7,3.048e+8,9.144e+8,1e+9,1e+12,1.609344e+12,1.852e+12},
            {1000.0, 10000.0, 25400.0, 304800.0, 914400.0,1e+6,1e+9,1.609344e+9,1.852e+9},
            {10.0, 25.4, 304.8, 914.40, 1000.0, 1e+6, 1.609344e+6, 1.852e+6},
            {2.54, 30.48, 91.44, 100.0, 100000.0, 160934.4, 185200.0},
            {12.0, 36.0, 39.37, 39370.078740157, 63360.0, 72913.38583},
            {3.0, 3.2808, 3280.8, 5280.0, 6076.12},
            {1.0936, 1093.6132983377, 1760.0, 2025.37},
            {1000.0, 1609.344, 1852.0},
            {1.609344, 1.852},
            {1.1507794}
    };
    private String[] lengthOrder={
            "Nanometer", "Micrometer","Millimeter","Centimeter",
            "Inch", "Foot","Yard", "Meter", "Kilometer","Mile", "Nautical Mile"
    };
    private Double[][] volumeValues = {
            {4.929, 5.919390467, 14.79, 16.387, 17.7582, 28.413, 29.57, 240.0, 284.0, 473.18, 568.26, 946.35, 1000.0, 1137.0, 3785.41, 4546.09, 28316.85, 1e+6},
            {1.201, 3.0, 3.325, 3.603, 5.765, 6.0, 48.692, 57.646, 96.0, 115.0, 192.0, 203.0, 231.0, 768.0, 922.0, 5745.0, 202884.0},
            {2.498, 2.768, 3.0, 4.8, 4.996, 40.545, 48.0, 79.937, 96.0, 160.0, 169.0,  192.0, 639.0, 768.0, 4784.0, 168936.0},
            {1.108, 1.201,  1.922, 2.0, 16.231, 19.215, 38.43, 64.0, 67.628, 76.861, 256.0, 307.0,  1915.0, 67628.0},
            {1.084, 1.734, 1.805, 14.646, 17.339, 28.875, 34.677,  57.75, 61.024, 69.355, 231.0, 1728.0, 61024.0},
            {1.6, 1.665, 13.515, 16.0,  26.646, 53.291, 56.312, 64.0,  213.0,  256.0, 1595.0, 56312.0},
            {1.041, 8.447, 10.0, 16.653, 20.0, 33.307, 35.195, 40.0, 133.0, 160.0, 997.0, 35195.0},
            {8.115, 9.608, 16.0, 19.215,  32.0, 33.814, 38.43, 128.0, 154.0, 958.0, 33814.0},
            {1.184, 1.972, 2.368, 3.943, 4.167, 4.736, 15.773, 18.942, 118.0, 4167.0},
            {1.665, 2.0, 3.331, 3.52, 4.0, 13.323, 16.0, 99.661, 3520.0},
            {1.201, 2.0, 2.113, 2.402, 8.0, 9.608, 59.844, 2113.0},
            {1.665, 1.76, 2.0, 6.661, 8.0, 49.831, 1760.0},
            {1.057, 1.201, 4.0, 4.804, 29.922,1057.0},
            {1.137, 3.785, 4.546, 28.317, 1000.0},
            {3.331, 4.0, 24.915, 880.0},
            {1.201, 7.481, 264.0},
            {6.229, 220.0},
            {35.315}
    };
    private String[] volumeOrder={
        "Milliliter", "U.S. Teaspoon", "Imperial Teaspoon", "U.S. Tablespoon", "Cubic Inch","Imperial Tablespoon",
        "Imperial Fluid Ounce","U.S. Fluid Ounce", "U.S. Cup", "Imperial Cup", "U.S. Pint", "Imperial Pint",
        "U.S. Quart", "Liter", "Imperial Quart", "U.S. Gallon", "Imperial Gallon", "Cubic Foot"
        ,"Cubic Meeter"
    };
    private Double[][] areaValues={
            {144.0, 1296.0, 1550.0031, 6.27264e+6, 1.55e+7, 1.55e+9, 4.0144896e+9},
            {9.0, 10.764, 43560.0, 107639.10417, 1.076391041671e+7, 2.788e+7},
            {1.196, 4840.0, 11959.9005, 1.196e+6, 3.0976e+6},
            {4046.8564224, 10000.0, 1e+6, 2.59e+6},
            {2.471, 247.11, 640.0},
            {100.0, 258.99881103},
            {2.59}
    };
    private String[] areaOrder={
            "Square Inch", "Square Foot", "Square Yard", "Square Meter","Acre","Hectare","Square Kilometer","Square Mile"
    };
    private Double[][] speedValues={
            {1.0973, 1.609, 1.852, 3.6},
            {1.4667, 1.6878, 3.2808},
            {1.1508, 2.237},
            {1.9438}
    };
    private String[] speedOrder={
            "Kilometer Per Hour", "Foot Per Second", "Miles Per Hour", "Knot", "Meter Per Second"
    };
    private Double[][] digitalStorageValues = {
            {8.0, 1000.0, 8000.0, 1e+6, 8e+6, 1e+9, 8e+9, 1e+12, 8e+12, 1e+15, 8e+15},
            {125.0, 1000.0, 125000.0, 1e+6, 1.25e+8, 1e+9, 1.25e+11, 1e+12, 1.25e+14, 1e+15},
            {8.0, 1000.0, 8000.0, 1e+6, 8e+6, 1e+9, 8e+9, 1e+12, 8e+12},
            {125.0, 1000.0, 125000.0, 1e+6, 1.25e+8, 1e+9, 1.25e+11, 1e+12},
            {8.0, 1000.0, 8000.0, 1e+6, 8e+6, 1e+9, 8e+9},
            {125.0, 1000.0, 125000.0, 1e+6, 1.25e+8, 1e+9},
            {8.0, 1000.0, 8000.0, 1e+6, 8e+6},
            {125.0, 1000.0, 125000.0, 1e+6},
            {8.0, 1000.0, 8000.0},
            {125.0, 1000.0},
            {8.0}
    };
    private String[] digitalStorageOrder = {
            "Bit", "Byte", "Kilobit", "Kilobyte", "Megabit", "Megabyte", "Gigabit", "Gigabyte", "Terabit", "Terabyte",
            "Petabit", "Petabyte"
    };
    private Double[][] digitalTransferValues={
            {1000.0, 8000.0, 1e+6, 8e+6, 1e+9, 8e+9, 1e+12, 8e+12},
            {8.0, 1000.0, 8000.0, 1e+6, 8e+6, 1e+9, 8e+9},
            {125.0, 1000.0, 125000.0, 1e+6, 1.25e+8, 1e+9},
            {8.0, 1000.0, 8000.0, 1e+6, 8e+6},
            {125.0, 1000.0, 125000.0, 1e+6},
            {8.0, 1000.0, 8000.0},
            {125.0, 1000.0},
            {8.0}
    };
    private String[] digitalTransferOrder={
            "Bit Per Second", "Kilobit Per Second", "Kilobyte Per Second", "Megabit Per Second", "Megabyte Per Second",
            "Gigabit Per Second", "Gigabyte Per Second", "Terabit Per Second", "Terabyte Per Second"
    };
    private Double[][] massValues={
            {1000.0, 1e+6, 2.835e+7, 4.536e+8, 1e+9, 6.35e+9, 9.072e+11, 1e+12, 1.016e+12},
            {1000.0, 28349.52, 453592.33, 1e+6, 6.35e+6, 9.07184740e+8, 1e+9, 1.016e+9},
            {28.35, 453.59237, 1000.0, 6350.29, 907184.75, 1e+6, 1.016e+6},
            {16.0, 35.274, 224.0, 32000.0, 35273.96, 35840.0},
            {2.205, 14.0, 2000.0, 2204.6, 2240.0},
            {6.35029318, 907.18474, 1000.0, 1016.0469088},
            {142.85714286, 157.47, 160.0},
            {1.102, 1.12},
            {1.016}
    };
    private String[] massOrder={
            "Microgram", "Milligram", "Gram", "Ounce", "Pound", "Kilogram", "Stone", "U.S. Ton", "Metric Ton",
            "Imperial Ton"
    };
    private Double[][] frequencyValues = {
            {1000.0, 1e+6, 1e+9},
            {1000.0, 1e+6},
            {1000.0}
    };
    private String[] frequencyOrder = {
            "Hertz", "Kilohertz", "Megahertz", "Gigahertz"
    };
    public double convertFrequency(double value, String firstUnit, String secondUnit){
        int x = 0;
        int y =0;
        for(int i = 0; i < frequencyOrder.length; i++){
            if(firstUnit.equals(frequencyOrder[i])){
                x = i;
            }
            if(secondUnit.equals(frequencyOrder[i])){
                y = i;
            }
        }
        if(x == y){
            return value;
        }
        if(x < y){
            return value/frequencyValues[x][y-(x+1)];
        }else{
            return value*frequencyValues[y][x-(y+1)];
        }
    }
    public double convertDigitalTransfer(double value, String firstUnit, String secondUnit){
        int x = 0;
        int y =0;
        for(int i = 0; i < digitalTransferOrder.length; i++){
            if(firstUnit.equals(digitalTransferOrder[i])){
                x = i;
            }
            if(secondUnit.equals(digitalTransferOrder[i])){
                y = i;
            }
        }
        if(x == y){
            return value;
        }
        if(x < y){
            return value/digitalTransferValues[x][y-(x+1)];
        }else{
            return value*digitalTransferValues[y][x-(y+1)];
        }
    }
    public double convertDigitalStorage(double value, String firstUnit, String secondUnit){
        int x = 0;
        int y =0;
        for(int i = 0; i < digitalStorageOrder.length; i++){
            if(firstUnit.equals(digitalStorageOrder[i])){
                x = i;
            }
            if(secondUnit.equals(digitalStorageOrder[i])){
                y = i;
            }
        }
        if(x == y){
            return value;
        }
        if(x < y){
            return value/digitalStorageValues[x][y-(x+1)];
        }else{
            return value*digitalStorageValues[y][x-(y+1)];
        }
    }
    public double convertMass(double value, String firstUnit, String secondUnit){
        int x = 0;
        int y =0;
        for(int i = 0; i < massOrder.length; i++){
            if(firstUnit.equals(massOrder[i])){
                x = i;
            }
            if(secondUnit.equals(massOrder[i])){
                y = i;
            }
        }
        if(x == y){
            return value;
        }
        if(x < y){
            return value/massValues[x][y-(x+1)];
        }else{
            return value*massValues[y][x-(y+1)];
        }
    }
    public double convertLength(double value, String firstUnit, String secondUnit){
        int x = 0;
        int y =0;
        for(int i = 0; i < lengthOrder.length; i++){
            if(firstUnit.equals(lengthOrder[i])){
                x = i;
            }
            if(secondUnit.equals(lengthOrder[i])){
                y = i;
            }
        }
        if(x == y){
            return value;
        }
        if(x < y){
            return value/lengthValues[x][y-(x+1)];
        }else{
            return value*lengthValues[y][x-(y+1)];
        }
    }
    public double convertVolume(double value, String firstUnit, String secondUnit){
        int x = 0;
        int y =0;
        for(int i = 0; i < volumeOrder.length; i++){
            if(firstUnit.equals(volumeOrder[i])){
                x = i;
            }
            if(secondUnit.equals(volumeOrder[i])){
                y = i;
            }
        }
        if(x == y){
            return value;
        }
        if(x < y){
            return value/volumeValues[x][y-(x+1)];
        }else{
            return value*volumeValues[y][x-(y+1)];
        }
    }
    public double convertArea(double value, String firstUnit, String secondUnit){
        int x = 0;
        int y =0;
        for(int i = 0; i < areaOrder.length; i++){
            if(firstUnit.equals(areaOrder[i])){
                x = i;
            }
            if(secondUnit.equals(areaOrder[i])){
                y = i;
            }
        }
        if(x == y){
            return value;
        }
        if(x < y){
            return value/areaValues[x][y-(x+1)];
        }else{
            return value*areaValues[y][x-(y+1)];
        }
    }
    public double convertSpeed(double value, String firstUnit, String secondUnit){
        int x = 0;
        int y =0;
        for(int i = 0; i < speedOrder.length; i++){
            if(firstUnit.equals(speedOrder[i])){
                x = i;
            }
            if(secondUnit.equals(speedOrder[i])){
                y = i;
            }
        }
        if(x == y){
            return value;
        }
        if(x < y){
            return value/speedValues[x][y-(x+1)];
        }else{
            return value*speedValues[y][x-(y+1)];
        }
    }
    public double convertTemp(double value, String firstUnit, String secondUnit){
        switch (firstUnit){
            case "Fahrenheit":
                return convertFah(value, secondUnit);
            case "Celsius":
                return convertCel(value, secondUnit);
            case "Kelvin":
                return convertKel(value, secondUnit);
            default:break;
        }
        return value;
    }
    private double convertCel(Double value, String secondUnit){
        switch (secondUnit){
            case "Kelvin":
                return (value +273.15);
            case "Fahrenheit":
                return (value* 9/5)+32;
            default:break;
        }
        return value;
    }
    private double convertFah(Double value, String secondUnit){
        switch (secondUnit){
            case "Kelvin":
                return (value -32)*5/9 + 273.15 ;
            case "Celsius":
                return (value -32)*5/9;
            default:break;
        }
        return value;
    }
    private double convertKel(Double value, String secondUnit){
        switch (secondUnit){
            case "Fahrenheit":
                return (value -273.15)*9/5 + 32;
            case "Celsius":
                return (value -273.15);
            default:break;
        }
        return value;
    }
    public double convertAngle(double value, String firstUnit, String secondUnit){
        switch (firstUnit){
            case "Radian":
                return convertRad(value, secondUnit);
            case "Degree":
                return convertDegree(value, secondUnit);
            case "Gradian":
                return convertGradian(value, secondUnit);
            case "Milliradian":
                return convertMilliradian(value, secondUnit);
            case "Minute of Arc":
                return convertMinuteOfArc(value, secondUnit);
            case "Second of Arc":
                return convertSecondOfArc(value, secondUnit);
            default:break;
        }
        return value;
    }
    private double convertRad(double value, String secondUnit){
        switch (secondUnit){
            case "Gradian":
                return value*(200.0/Math.PI);
            case "Degree":
                return value*(180.0/Math.PI);
            case "Milliradian":
                return value*1000.0;
            case "Minute of Arc":
                return value*((60.0*180.0)/Math.PI);
            case "Second of Arc":
                return value*((3600.0*180.0)/Math.PI);
            default:break;
        }
        return value;
    }
    private double convertDegree(double value, String secondUnit){
        switch (secondUnit){
            case "Gradian":
                return value*(200.0/180.0);
            case "Radian":
                return value*(Math.PI/180.0);
            case "Milliradian":
                return value*(1000*Math.PI/180.0);
            case "Minute of Arc":
                return value*60.0;
            case "Second of Arc":
                return value*3600.0;
            default:break;
        }
        return value;
    }
    private double convertGradian(double value, String secondUnit){
        switch (secondUnit){
            case "Degree":
                return value*(180.0/200.0);
            case "Radian":
                return value*(Math.PI/200.0);
            case "Milliradian":
                return value*(1000*Math.PI/200.0);
            case "Minute of Arc":
                return value*54.0;
            case "Second of Arc":
                return value*3240.0;
            default:break;
        }
        return value;
    }
    private double convertMilliradian(double value, String secondUnit){
        switch (secondUnit){
            case "Gradian":
                return value*(200.0/(1000.0*Math.PI));
            case "Radian":
                return value/1000.0;
            case "Degree":
                return value*(180.0/1000*Math.PI);
            case "Minute of Arc":
                return value*(60.0*180.0)/(1000.0*Math.PI) ;
            case "Second of Arc":
                return value*((3600.0*180.0)/(1000.0*Math.PI));
            default:break;
        }
        return value;
    }
    private double convertMinuteOfArc(double value, String secondUnit){
        switch (secondUnit){
            case "Gradian":
                return value/54.0;
            case "Radian":
                return value*(Math.PI/(60.0 * 180.0));
            case "Degree":
                return value/60.0;
            case "Milliradian":
                return value*(1000.0*Math.PI)/(60.0*180.0) ;
            case "Second of Arc":
                return value*60.0;
            default:break;
        }
        return value;
    }
    private double convertSecondOfArc(double value, String secondUnit){
        switch (secondUnit){
            case "Gradian":
                return value/3240.0;
            case "Radian":
                return value*(Math.PI/(3600.0 * 180.0));
            case "Degree":
                return value*3600.0;
            case "Milliradian":
                return value*(1000.0*Math.PI)/(3600.0*180.0) ;
            case "Minute of Arc":
                return value/60.0;
            default:break;
        }
        return value;
    }
}
